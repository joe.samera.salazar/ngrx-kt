import { Component, OnInit } from '@angular/core';
import { Store } from '@ngrx/store';
import { Observable } from 'rxjs';
import { cartAddItem } from '../store/app.actions';
import { selectProducts } from '../store/app.selectors';
import { Product } from '../store/model/app.state';

@Component({
  selector: 'app-product-list',
  templateUrl: './product-list.component.html',
  styleUrls: ['./product-list.component.css']
})
export class ProductListComponent implements OnInit {

  public products$: Observable<ReadonlyArray<Product>> | null = null;

  constructor(private store: Store) { }

  ngOnInit(): void {
     this.products$ = this.store.select(selectProducts); 
  }

  addToCart(product: Product) {
    this.store.dispatch(cartAddItem({ product }));
  }
}
