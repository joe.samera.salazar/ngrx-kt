import { Component, OnInit } from '@angular/core';
import { Store } from '@ngrx/store';
import { loadCategories, LOAD_CATEGORIES } from './store/app.actions';
import { AppState } from './store/model/app.state';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  title = 'ngrx-kt';

  constructor(private store: Store<AppState>) { }

  ngOnInit(): void {
    this.store.dispatch(loadCategories());
  }
}
