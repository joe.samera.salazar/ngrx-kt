import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Category } from './model/category';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class CategoryService {
  private readonly url: string = "http://localhost:3000/categories";

  constructor(private http: HttpClient) { }

  loadCategories(): Observable<Category[]> {
    return this.http.get<Category>(this.url)
      .pipe(
        map((data: any) => data)
      );
  }
}
