import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { HttpClientModule } from '@angular/common/http';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { CategoryListComponent } from './category-list/category-list.component';
import { SortOptionsComponent } from './sort-options/sort-options.component';
import { ProductListComponent } from './product-list/product-list.component';
import { CartWidgetComponent } from './cart-widget/cart-widget.component';
import { HomeComponent } from './home/home.component';
import { CartViewComponent } from './cart-view/cart-view.component';
import { StoreModule } from '@ngrx/store';
import { AppReducer } from './store/app.reducers';
import { StoreDevtoolsModule } from '@ngrx/store-devtools';
import { environment } from 'src/environments/environment';
import { EffectsModule } from '@ngrx/effects';
import { AppEffects } from './store/app.effects';

@NgModule({
  declarations: [
    AppComponent,
    CategoryListComponent,
    SortOptionsComponent,
    ProductListComponent,
    CartWidgetComponent,
    HomeComponent,
    CartViewComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    StoreModule.forRoot({ appState: AppReducer }),
    StoreDevtoolsModule.instrument({ maxAge: 25, logOnly: environment.production }),
    EffectsModule.forRoot([AppEffects])
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
