import { Component, OnInit } from '@angular/core';
import { Store } from '@ngrx/store';
import { Observable } from 'rxjs';
import { filterProducts } from '../store/app.actions';
import { selectCategories } from '../store/app.selectors';
import { Category } from '../store/model/app.state';

@Component({
  selector: 'app-category-list',
  templateUrl: './category-list.component.html',
  styleUrls: ['./category-list.component.css']
})
export class CategoryListComponent implements OnInit {
  public categories$: Observable<ReadonlyArray<Category>> | null = null;

  constructor(private store: Store) { }

  ngOnInit(): void {
   this.categories$ = this.store.select(selectCategories);
  }

  onClick(categoryId: number) {
    this.store.dispatch(filterProducts({ categoryId }));
  }
}
