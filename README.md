# NgrxKt

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 12.1.4.

## Development server

Run `npm install` to install package dependencies
Run `npm run db-server` to run the mocked API
Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.`

## master branch - contains the full solution
## dev branch - if you want to implement ngrx from scratch

